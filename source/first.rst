.. _firstpage:

GSoC INCF
==========

Project Measure quality of Cerebunit Validation tests
------------------------------------------------------
* cerebtests
* cerebstats

Steps
------
1. **Learn** 
2. *Design*
3. "Code"
4. Document
5. Test
6. Review

.. figure:: /images/GSoC-icon.svg.png
   :alt: GSoC Image
   :scale: 30%
   :align: center

   *Fig. Google Summer of Code 2021*